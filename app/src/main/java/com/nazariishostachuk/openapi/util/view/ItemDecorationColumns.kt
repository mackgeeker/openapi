package com.nazariishostachuk.openapi.util.view

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View
import com.nazariishostachuk.openapi.App
import com.nazariishostachuk.openapi.R
import com.nazariishostachuk.openapi.ui.global.BaseItemStyle

class ItemDecorationColumns(
    private val mSizeGridSpacingPx: Int,
    private var style: BaseItemStyle?
) :
    RecyclerView.ItemDecoration() {
    private var mNeedLeftSpacing = false

    private val columnsSize: Int
        get() = if (style === BaseItemStyle.LIST) 1 else App.i.resources.getInteger(R.integer.photos_columns_size)

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val frameWidth = ((parent.width - mSizeGridSpacingPx.toFloat() * (columnsSize - 1)) / columnsSize).toInt()
        val padding = parent.width / columnsSize - frameWidth
        val itemPosition = (view.layoutParams as RecyclerView.LayoutParams).viewAdapterPosition
        if (itemPosition < columnsSize) {
            outRect.top = 0
        } else {
            outRect.top = mSizeGridSpacingPx
        }
        if (itemPosition % columnsSize == 0) {
            outRect.left = 0
            outRect.right = padding
            mNeedLeftSpacing = true
        } else if ((itemPosition + 1) % columnsSize == 0) {
            mNeedLeftSpacing = false
            outRect.right = 0
            outRect.left = padding
        } else if (mNeedLeftSpacing) {
            mNeedLeftSpacing = false
            outRect.left = mSizeGridSpacingPx - padding
            if ((itemPosition + 2) % columnsSize == 0) {
                outRect.right = mSizeGridSpacingPx - padding
            } else {
                outRect.right = mSizeGridSpacingPx / 2
            }
        } else if ((itemPosition + 2) % columnsSize == 0) {
            mNeedLeftSpacing = false
            outRect.left = mSizeGridSpacingPx / 2
            outRect.right = mSizeGridSpacingPx - padding
        } else {
            mNeedLeftSpacing = false
            outRect.left = mSizeGridSpacingPx / 2
            outRect.right = mSizeGridSpacingPx / 2
        }
        outRect.bottom = 0
    }

    fun setStyle(style: BaseItemStyle) {
        this.style = style
    }
}
