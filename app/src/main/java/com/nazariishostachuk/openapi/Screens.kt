package com.nazariishostachuk.openapi

import android.support.v4.app.Fragment
import com.nazariishostachuk.openapi.ui.photo.PhotoDetailsFragment
import com.nazariishostachuk.openapi.ui.photos.PhotosFragment
import com.nazariishostachuk.openapi.ui.post.PostDetailsFragment
import com.nazariishostachuk.openapi.ui.posts.PostsFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {
    class PHOTOS_SCREEN : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return PhotosFragment()
        }
    }

    class PHOTO_DETAILS_SCREEN(val id: String) : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return PhotoDetailsFragment.createInstance(id)
        }
    }

    class POSTS_SCREEN : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return PostsFragment()
        }
    }

    class POST_DETAILS_SCREEN(val id: String) : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return PostDetailsFragment.createInstance(id)
        }
    }
}