package com.nazariishostachuk.openapi

import android.app.Application
import io.realm.Realm


class App : Application() {

    private lateinit var dependencies: Dependencies

    companion object {
        lateinit var i: App
    }

    override fun onCreate() {
        super.onCreate()
        i = this
        dependencies = Dependencies(this)
        Realm.init(this)
    }

    fun DI() = dependencies
}