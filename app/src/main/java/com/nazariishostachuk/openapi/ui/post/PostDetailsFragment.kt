package com.nazariishostachuk.openapi.ui.post

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.nazariishostachuk.openapi.App
import com.nazariishostachuk.openapi.R
import com.nazariishostachuk.openapi.changeVisibility
import com.nazariishostachuk.openapi.model.entity.net.comments.CommentResult
import com.nazariishostachuk.openapi.presentation.post.PostDetailsPresenter
import com.nazariishostachuk.openapi.presentation.post.PostDetailsView
import com.nazariishostachuk.openapi.ui.global.BaseItemStyle
import com.nazariishostachuk.openapi.ui.post.adapter.CommentsAdapter
import com.nazariishostachuk.openapi.util.view.ItemDecorationColumns

const val POST_ID_KEY = "post_id_key"

class PostDetailsFragment : MvpAppCompatFragment(), PostDetailsView, SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun createInstance(id: String): PostDetailsFragment {
            val bundle = Bundle().apply {
                putString(POST_ID_KEY, id)
            }
            val fragment = PostDetailsFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var postTitle: TextView
    private lateinit var backButton: View
    private lateinit var postBody: TextView
    private lateinit var authorName: TextView
    private lateinit var authorAvatar: ImageView
    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var toolbar: Toolbar
    private lateinit var noInternet: View
    private lateinit var rv: RecyclerView
    private lateinit var commentsTitleView: View
    private lateinit var adapter: CommentsAdapter

    private val glide = App.i.DI().glide

    @InjectPresenter
    lateinit var presenter: PostDetailsPresenter

    @ProvidePresenter
    fun providePresenter() =
        PostDetailsPresenter(
            arguments?.getString(POST_ID_KEY, null),
            App.i.DI().router,
            App.i.DI().postsInteractor,
            App.i.DI().commentsInteractor,
            App.i.DI().errorHandler
        )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_post_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        postTitle = view.findViewById(R.id.fragment_post_details_title)
        postBody = view.findViewById(R.id.fragment_post_details_body)
        commentsTitleView = view.findViewById(R.id.fragment_post_details_comment_text)
        authorName = view.findViewById(R.id.fragment_post_details_author_name)
        authorAvatar = view.findViewById(R.id.fragment_post_details_author_avatar)
        refreshLayout = view.findViewById(R.id.refreshLayout)
        toolbar = view.findViewById(R.id.toolbar)
        backButton = view.findViewById(R.id.toolbar_navigation_button)
        noInternet = view.findViewById(R.id.noInternetLayout)
        rv = view.findViewById(R.id.comments_rv)
    }

    override fun onRefresh() {
        presenter.getPostDetails()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapter = CommentsAdapter()
        rv.layoutManager = object : LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
        rv.preserveFocusAfterLayout = false
        rv.addItemDecoration(
            ItemDecorationColumns(
                resources.getDimensionPixelSize(R.dimen.photos_list_spacing),
                BaseItemStyle.LIST
            )
        )
        rv.adapter = adapter
        refreshLayout.setOnRefreshListener(this)
        backButton.setOnClickListener {
            presenter.onBackClicked()
        }
    }

    override fun showComments(comments: List<CommentResult>) {
        commentsTitleView.changeVisibility(View.VISIBLE)
        adapter.insertItems(comments)
    }

    override fun showLoading(enable: Boolean) {
        refreshLayout.isRefreshing = enable
    }

    override fun setPostDetails(title: String, body: String) {
        noInternet.changeVisibility(View.GONE)
        postTitle.text = title
        postBody.text = body
    }

    override fun setAuthor(avatarUrl: String, name: String) {
        glide.load(avatarUrl).into(authorAvatar)
        authorName.text = name
    }

    override fun showErrorView() {
        noInternet.changeVisibility(View.VISIBLE)
    }
}