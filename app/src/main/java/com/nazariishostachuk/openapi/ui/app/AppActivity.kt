package com.nazariishostachuk.openapi.ui.app

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.nazariishostachuk.openapi.App
import com.nazariishostachuk.openapi.R
import com.nazariishostachuk.openapi.presentation.app.AppPresenter
import com.nazariishostachuk.openapi.presentation.app.AppView
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator

class AppActivity : MvpAppCompatActivity(), AppView {

    @InjectPresenter
    lateinit var presenter: AppPresenter

    @ProvidePresenter
    fun providePresenter() = AppPresenter(
        App.i.DI().router
    )

    private var navigatorHolder: NavigatorHolder = App.i.DI().navigatorHolder

    private val navigator = object : SupportAppNavigator(this, R.id.app_fragment_container) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app)

        if (savedInstanceState == null)
            presenter.onAppLoaded()
    }

    override fun setBackgroundColor() {
        window.decorView.setBackgroundResource(R.color.color_default_background)
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

}