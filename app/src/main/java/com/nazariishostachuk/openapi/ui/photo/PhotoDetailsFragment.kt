package com.nazariishostachuk.openapi.ui.photo

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.nazariishostachuk.openapi.App
import com.nazariishostachuk.openapi.R
import com.nazariishostachuk.openapi.model.entity.net.user.UserResult
import com.nazariishostachuk.openapi.presentation.photo.PhotoDetailsPresenter
import com.nazariishostachuk.openapi.presentation.photo.PhotoDetailsView

const val PHOTO_ID_KEY = "photo_id_key"

class PhotoDetailsFragment : MvpAppCompatFragment(), PhotoDetailsView, SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun createInstance(id: String): PhotoDetailsFragment {
            val bundle = Bundle().apply {
                putString(PHOTO_ID_KEY, id)
            }
            val fragment = PhotoDetailsFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    @InjectPresenter
    lateinit var presenter: PhotoDetailsPresenter
    private val glide = App.i.DI().glide
    private lateinit var image: ImageView
    private lateinit var title: TextView
    private lateinit var authorAvatar: ImageView
    private lateinit var authorName: TextView
    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var toolbar: Toolbar

    @ProvidePresenter
    fun providePresenter() = PhotoDetailsPresenter(
        arguments?.getString(PHOTO_ID_KEY),
        App.i.DI().router,
        App.i.DI().photoInteractor,
        App.i.DI().errorHandler
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_photo_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        image = view.findViewById(R.id.fragment_photo_details_image)
        title = view.findViewById(R.id.fragment_photo_details_title)
        authorAvatar = view.findViewById(R.id.fragment_photo_details_author_avatar)
        authorName = view.findViewById(R.id.fragment_photo_details_author_name)
        refreshLayout = view.findViewById(R.id.refreshLayout)
        toolbar = view.findViewById(R.id.toolbar)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        refreshLayout.setOnRefreshListener(this)
        toolbar.setNavigationOnClickListener {
            presenter.onBackClicked()
        }
    }

    override fun onRefresh() {
        presenter.loadPhotoData()
    }

    override fun showLoading(enable: Boolean) {
        refreshLayout.isRefreshing = enable
    }

    override fun loadPhoto(url: String) {
        glide.load(url).into(image)
    }

    override fun setTitle(title: String) {
        this.title.text = title
    }

    override fun setAuthor(user: UserResult) {
        glide.load(user._links.avatar.href).into(authorAvatar)
        authorName.text = "By ${user.name}"
    }

}