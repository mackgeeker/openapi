package com.nazariishostachuk.openapi.ui.post.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.nazariishostachuk.openapi.R
import com.nazariishostachuk.openapi.model.entity.net.comments.CommentResult

class CommentsAdapter : RecyclerView.Adapter<CommentsAdapter.CommentViewHolder>() {

    private val items = mutableListOf<CommentResult>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CommentViewHolder =
        CommentViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_comment, p0, false))

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: CommentViewHolder, p1: Int) {
        val item = items[p1]

        holder.body.text = item.body
        holder.title.text = item.name
        holder.email.text = item.email
    }

    class CommentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById<TextView>(R.id.item_comment_title)
        val body = itemView.findViewById<TextView>(R.id.item_comment_body)
        val email = itemView.findViewById<TextView>(R.id.item_comment_secondary_text)
    }

    fun insertItems(items: List<CommentResult>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

}