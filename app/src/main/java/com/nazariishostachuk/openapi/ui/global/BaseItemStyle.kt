package com.nazariishostachuk.openapi.ui.global

enum class BaseItemStyle {
    LIST, GRID
}