package com.nazariishostachuk.openapi.ui.photos.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.RequestManager
import com.nazariishostachuk.openapi.R
import com.nazariishostachuk.openapi.model.entity.net.photo.Photo
import com.nazariishostachuk.openapi.ui.global.BaseItemStyle

class PhotosItemAdapter(
    private val glide: RequestManager,
    private val onClickListener: OnClickListener
) : RecyclerView.Adapter<PhotosItemAdapter.PhotoViewHolder>() {

    private val items = mutableListOf<Photo>()
    private var layoutStyle: BaseItemStyle? = null

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PhotoViewHolder =
        PhotoViewHolder(
            LayoutInflater.from(p0.context).inflate(
                handleLayoutStyle(), p0, false
            )
        )

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: PhotoViewHolder, p: Int) {
        val item = items[p]

        glide.load(item.thumbnail).into(holder.image)

        holder.title.text = item.title
        holder.author.text = item.album?.user?.name

        holder.root.setOnClickListener {
            onClickListener.onItemClicked(item)
        }
    }

    fun insertItems(items: List<Photo>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    private fun handleLayoutStyle(): Int =
        when (layoutStyle) {
            BaseItemStyle.GRID -> R.layout.item_photo_grid
            else -> R.layout.item_photo_list
        }

    fun setLayoutStyle(style: BaseItemStyle) {
        layoutStyle = style
    }

    fun getSelectedStyle() = layoutStyle

    override fun getItemViewType(position: Int): Int {
        return layoutStyle?.ordinal ?: BaseItemStyle.LIST.ordinal
    }

    class PhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image = itemView.findViewById<ImageView>(R.id.item_photo_image)
        val title = itemView.findViewById<TextView>(R.id.item_photo_title)
        val author = itemView.findViewById<TextView>(R.id.item_photo_author)
        val root = itemView.findViewById<View>(R.id.item_photo_root)
    }

    interface OnClickListener {
        fun onItemClicked(photo: Photo)
    }

}