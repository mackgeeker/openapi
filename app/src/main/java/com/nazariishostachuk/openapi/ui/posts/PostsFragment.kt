package com.nazariishostachuk.openapi.ui.posts

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.PopupMenu
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.nazariishostachuk.openapi.App
import com.nazariishostachuk.openapi.R
import com.nazariishostachuk.openapi.changeVisibility
import com.nazariishostachuk.openapi.model.entity.net.posts.PostResult
import com.nazariishostachuk.openapi.presentation.posts.PostsPresenter
import com.nazariishostachuk.openapi.presentation.posts.PostsView
import com.nazariishostachuk.openapi.ui.global.BaseItemStyle
import com.nazariishostachuk.openapi.ui.posts.adapter.PostsItemAdapter
import com.nazariishostachuk.openapi.util.view.ItemDecorationColumns

class PostsFragment : MvpAppCompatFragment(), PostsView, PostsItemAdapter.OnClickListener,
    SwipeRefreshLayout.OnRefreshListener, PopupMenu.OnMenuItemClickListener {

    @InjectPresenter
    lateinit var presenter: PostsPresenter

    @ProvidePresenter
    fun providePresenter() =
        PostsPresenter(
            App.i.DI().router,
            App.i.DI().postsInteractor,
            App.i.DI().errorHandler
        )

    private lateinit var adapter: PostsItemAdapter
    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var rv: RecyclerView
    private lateinit var menuButton: ImageButton
    private lateinit var gridItemDecoration: ItemDecorationColumns
    private lateinit var noInternet: View
    private val glide = App.i.DI().glide

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_posts, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUI()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = PostsItemAdapter(glide, this)
        refreshLayout = view.findViewById(R.id.refresh_layout)
        rv = view.findViewById(R.id.items_rv)
        noInternet = view.findViewById(R.id.noInternetLayout)
        menuButton = view.findViewById(R.id.toolbar_action_button)
    }

    private fun setupUI() {
        gridItemDecoration = ItemDecorationColumns(
            resources.getDimensionPixelSize(R.dimen.photos_list_spacing),
            BaseItemStyle.LIST
        )

        menuButton.setOnClickListener {
            val popup = PopupMenu(context, it)
            popup.setOnMenuItemClickListener(this)
            popup.inflate(R.menu.items_styles_menu)
            popup.show()
        }

        refreshLayout.setOnRefreshListener(this)
        rv.adapter = adapter
        adapter.setLayoutStyle(BaseItemStyle.LIST)
        rv.layoutManager = GridLayoutManager(context, 1)
        rv.addItemDecoration(gridItemDecoration)
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        var style: BaseItemStyle = BaseItemStyle.LIST

        when (item.itemId) {
            R.id.items_style_grid -> {
                style = BaseItemStyle.GRID
            }
        }
        presenter.onListStyleSelected(style)
        return true
    }

    override fun changeListStyle(style: BaseItemStyle) {
        if (adapter.getSelectedStyle() != style) {
            adapter.setLayoutStyle(style)
            clearItemDecorations()
            val cols = when (style) {
                BaseItemStyle.LIST -> 1
                else -> resources.getInteger(R.integer.photos_columns_size)
            }
            rv.layoutManager = GridLayoutManager(context, cols)

            gridItemDecoration.setStyle(style)
            rv.addItemDecoration(gridItemDecoration)
            adapter.notifyItemRangeChanged(0, adapter.itemCount)
        }
    }

    override fun onItemClicked(photo: PostResult) {
        presenter.onItemClicked(photo)
    }

    private fun clearItemDecorations() {
        for (i: Int in 0 until rv.itemDecorationCount)
            rv.removeItemDecorationAt(i)
    }

    override fun showPosts(items: List<PostResult>) {
        noInternet.changeVisibility(View.GONE)
        adapter.insertItems(items)
    }

    override fun showLoading(enable: Boolean) {
        refreshLayout.isRefreshing = enable
    }

    override fun onRefresh() {
        presenter.loadPosts()
    }

    override fun showErrorView() {
        noInternet.changeVisibility(View.VISIBLE)
    }
}