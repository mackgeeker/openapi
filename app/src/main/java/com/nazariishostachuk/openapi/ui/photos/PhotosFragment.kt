package com.nazariishostachuk.openapi.ui.photos

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.PopupMenu
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.nazariishostachuk.openapi.App
import com.nazariishostachuk.openapi.R
import com.nazariishostachuk.openapi.model.entity.net.photo.Photo
import com.nazariishostachuk.openapi.presentation.photos.PhotosPresenter
import com.nazariishostachuk.openapi.presentation.photos.PhotosView
import com.nazariishostachuk.openapi.ui.global.BaseItemStyle
import com.nazariishostachuk.openapi.ui.photos.adapter.PhotosItemAdapter
import com.nazariishostachuk.openapi.util.view.ItemDecorationColumns

class PhotosFragment : MvpAppCompatFragment(), PhotosView, PhotosItemAdapter.OnClickListener,
    SwipeRefreshLayout.OnRefreshListener,
    PopupMenu.OnMenuItemClickListener {

    @InjectPresenter
    lateinit var presenter: PhotosPresenter

    @ProvidePresenter
    fun providePresenter() =
        PhotosPresenter(
            App.i.DI().router,
            App.i.DI().photoInteractor,
            App.i.DI().errorHandler
        )

    private lateinit var adapter: PhotosItemAdapter
    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var rv: RecyclerView
    private lateinit var menuButton: ImageButton
    private lateinit var gridItemDecoration: ItemDecorationColumns
    private val glide = App.i.DI().glide

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_photos, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUI()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = PhotosItemAdapter(glide, this)
        refreshLayout = view.findViewById(R.id.refresh_layout)
        rv = view.findViewById(R.id.photos_rv)
        menuButton = view.findViewById(R.id.toolbar_action_button)
    }

    private fun setupUI() {
        gridItemDecoration = ItemDecorationColumns(
            resources.getDimensionPixelSize(R.dimen.photos_list_spacing),
            BaseItemStyle.LIST
        )

        menuButton.setOnClickListener {
            val popup = PopupMenu(context, it)
            popup.setOnMenuItemClickListener(this)
            popup.inflate(R.menu.items_styles_menu)
            popup.show()
        }

        refreshLayout.setOnRefreshListener(this)
        rv.adapter = adapter
        adapter.setLayoutStyle(BaseItemStyle.LIST)
        rv.layoutManager = GridLayoutManager(context, 1)
        rv.addItemDecoration(gridItemDecoration)
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        var style: BaseItemStyle = BaseItemStyle.LIST

        when (item.itemId) {
            R.id.items_style_grid -> {
                style = BaseItemStyle.GRID
            }
        }
        presenter.onListStyleSelected(style)
        return true
    }

    override fun changeListStyle(style: BaseItemStyle) {
        if (adapter.getSelectedStyle() != style) {
            adapter.setLayoutStyle(style)
            clearItemDecorations()
            val cols = when (style) {
                BaseItemStyle.LIST -> 1
                else -> resources.getInteger(R.integer.photos_columns_size)
            }
            rv.layoutManager = GridLayoutManager(context, cols)

            gridItemDecoration.setStyle(style)
            rv.addItemDecoration(gridItemDecoration)
            adapter.notifyItemRangeChanged(0, adapter.itemCount)
        }
    }

    override fun onItemClicked(photo: Photo) {
        presenter.onItemClicked(photo)
    }

    private fun clearItemDecorations() {
        for (i: Int in 0 until rv.itemDecorationCount)
            rv.removeItemDecorationAt(i)
    }

    override fun showPhotos(photos: List<Photo>) {
        adapter.insertItems(photos)
    }

    override fun showLoading(enable: Boolean) {
        refreshLayout.isRefreshing = enable
    }

    override fun onRefresh() {
        presenter.loadPhotos()
    }
}
