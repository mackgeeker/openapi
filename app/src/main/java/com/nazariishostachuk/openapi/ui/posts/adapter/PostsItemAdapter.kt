package com.nazariishostachuk.openapi.ui.posts.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.RequestManager
import com.nazariishostachuk.openapi.R
import com.nazariishostachuk.openapi.model.entity.net.posts.PostResult
import com.nazariishostachuk.openapi.ui.global.BaseItemStyle

class PostsItemAdapter(
    private val glide: RequestManager,
    private val onClickListener: OnClickListener
) : RecyclerView.Adapter<PostsItemAdapter.PostViewHolder>() {

    private val items = mutableListOf<PostResult>()
    private var layoutStyle: BaseItemStyle? = null

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PostViewHolder =
        PostViewHolder(
            LayoutInflater.from(p0.context).inflate(
                handleLayoutStyle(), p0, false
            )
        )

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: PostViewHolder, p: Int) {
        val item = items[p]

        item.user?.apply {
            glide.load(item.user!!._links.avatar.href).into(holder.authorAvatar)
            holder.authorName.text = item.user!!.name
        }

        holder.body.text = item.body
        holder.title.text = item.title

        holder.root.setOnClickListener {
            onClickListener.onItemClicked(item)
        }
    }

    fun insertItems(items: List<PostResult>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    private fun handleLayoutStyle(): Int =
        when (layoutStyle) {
            BaseItemStyle.GRID -> R.layout.item_post_grid
            else -> R.layout.item_post_list
        }

    fun setLayoutStyle(style: BaseItemStyle) {
        layoutStyle = style
    }

    fun getSelectedStyle() = layoutStyle

    override fun getItemViewType(position: Int): Int {
        return layoutStyle?.ordinal ?: BaseItemStyle.LIST.ordinal
    }

    class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById<TextView>(R.id.item_post_title)
        val body = itemView.findViewById<TextView>(R.id.item_post_body)
        val authorName = itemView.findViewById<TextView>(R.id.item_post_author_name)
        val authorAvatar = itemView.findViewById<ImageView>(R.id.item_post_author_avatar)
        val root = itemView.findViewById<View>(R.id.item_post_root)
    }

    interface OnClickListener {
        fun onItemClicked(photo: PostResult)
    }

}