package com.nazariishostachuk.openapi.presentation.app

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.nazariishostachuk.openapi.model.data.system.AddToEndSingleByTagStateStrategy

@StateStrategyType(AddToEndSingleByTagStateStrategy::class)
interface AppView : MvpView {
    fun setBackgroundColor()
}