package com.nazariishostachuk.openapi.presentation.post

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.nazariishostachuk.openapi.model.interactor.comments.CommentsInteractor
import com.nazariishostachuk.openapi.model.interactor.posts.PostsInteractor
import com.nazariishostachuk.openapi.presentation.base.BasePresenter
import com.nazariishostachuk.openapi.presentation.base.ErrorHandler
import ru.terrakok.cicerone.Router

@InjectViewState
class PostDetailsPresenter(
    private val post_id: String?,
    private val router: Router,
    private val postsInteractor: PostsInteractor,
    private val commentsInteractor: CommentsInteractor,
    private val errorHandler: ErrorHandler
) : BasePresenter<PostDetailsView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        getPostDetails()
    }

    fun getPostDetails() {
        if (post_id == null) router.exit()
        postsInteractor.getPostById(post_id!!)
            .doOnSubscribe {
                viewState.showLoading(true)
            }
            .doAfterTerminate {
                viewState.showLoading(false)
            }
            .subscribe({ t ->
                viewState.setPostDetails(t.title, t.body)
                t.user?.apply {
                    viewState.setAuthor(_links.avatar.href, name)
                }
                loadComments()
            }, { t ->
                errorHandler.proceed(t) { message, noInternet ->
                    if (noInternet) viewState.showErrorView()

                }
            })
            .connect()
    }

    private fun loadComments() {
        if (post_id == null) router.exit()
        commentsInteractor.getCommentsByPostId(post_id!!)
            .subscribe({ t ->
                if (t.isNotEmpty())
                    viewState.showComments(t)
            }, {
                errorHandler.proceed(it) { message, noInternet ->
                    Log.e("AAAAAAF", it.message)
                }
            })
            .connect()
    }

    fun onBackClicked() {
        router.exit()
    }

}