package com.nazariishostachuk.openapi.presentation.post

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.nazariishostachuk.openapi.model.data.system.AddToEndSingleByTagStateStrategy
import com.nazariishostachuk.openapi.model.entity.net.comments.CommentResult

@StateStrategyType(AddToEndSingleByTagStateStrategy::class)
interface PostDetailsView : MvpView {
    fun showLoading(enable: Boolean)
    fun showErrorView()
    fun setAuthor(avatarUrl: String, name: String)
    fun showComments(comments: List<CommentResult>)
    fun setPostDetails(title: String, body: String)
}