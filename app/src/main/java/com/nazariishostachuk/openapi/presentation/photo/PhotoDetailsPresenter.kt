package com.nazariishostachuk.openapi.presentation.photo

import com.arellomobile.mvp.InjectViewState
import com.nazariishostachuk.openapi.model.interactor.photo.PhotoInteractor
import com.nazariishostachuk.openapi.presentation.base.BasePresenter
import com.nazariishostachuk.openapi.presentation.base.ErrorHandler
import ru.terrakok.cicerone.Router

@InjectViewState
class PhotoDetailsPresenter(
    private val photo_id: String?,
    private val router: Router,
    private val photoInteractor: PhotoInteractor,
    private val errorHandler: ErrorHandler
) : BasePresenter<PhotoDetailsView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadPhotoData()
    }

    fun loadPhotoData() {
        if (photo_id == null) router.exit()
        photoInteractor.getPhotoById(photo_id = photo_id!!)
            .doOnSubscribe {
                viewState.showLoading(true)
            }
            .doAfterTerminate {
                viewState.showLoading(false)
            }
            .subscribe({ t ->
                viewState.loadPhoto(t.url)
                viewState.setTitle(t.title)
                t.album?.user?.apply {
                    viewState.setAuthor(t.album!!.user!!)
                }
            },
                { t ->
                    errorHandler.proceed(t) { message, noInternet ->

                    }
                })
            .connect()
    }

    fun onBackClicked() {
        router.exit()
    }

}