package com.nazariishostachuk.openapi.presentation.base

import com.nazariishostachuk.openapi.R
import com.nazariishostachuk.openapi.model.data.system.DataNotExistException
import com.nazariishostachuk.openapi.model.data.system.ResourceManager
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject

class ErrorHandler @Inject
constructor(private val resourceManager: ResourceManager) {

    fun proceed(error: Throwable, onErrorHandled: (message: String, noInternet: Boolean) -> Unit) {
        var message: String? = error.message
        var noInternet = false

        when (error) {
            is HttpException,
            is UnknownHostException,
            is IOException,
            is DataNotExistException,
            is SocketTimeoutException -> {
                noInternet = true
                message = resourceManager.getString(R.string.no_internet_connection)
            }
        }

        message?.apply {
            onErrorHandled(this, noInternet)
        }
    }

}
