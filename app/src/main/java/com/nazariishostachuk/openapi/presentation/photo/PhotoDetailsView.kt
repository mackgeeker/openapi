package com.nazariishostachuk.openapi.presentation.photo

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.nazariishostachuk.openapi.model.data.system.AddToEndSingleByTagStateStrategy
import com.nazariishostachuk.openapi.model.entity.net.user.UserResult

@StateStrategyType(AddToEndSingleByTagStateStrategy::class)
interface PhotoDetailsView : MvpView {
    fun showLoading(enable: Boolean)
    fun loadPhoto(url: String)
    fun setTitle(title: String)
    fun setAuthor(user: UserResult)
}