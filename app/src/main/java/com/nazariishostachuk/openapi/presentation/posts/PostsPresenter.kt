package com.nazariishostachuk.openapi.presentation.posts

import com.arellomobile.mvp.InjectViewState
import com.nazariishostachuk.openapi.Screens
import com.nazariishostachuk.openapi.model.entity.net.posts.PostResult
import com.nazariishostachuk.openapi.model.interactor.posts.PostsInteractor
import com.nazariishostachuk.openapi.presentation.base.BasePresenter
import com.nazariishostachuk.openapi.presentation.base.ErrorHandler
import com.nazariishostachuk.openapi.ui.global.BaseItemStyle
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class PostsPresenter @Inject constructor(
    private val router: Router,
    private val postsInteractor: PostsInteractor,
    private val errorHandler: ErrorHandler
) : BasePresenter<PostsView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadPosts()
    }

    fun loadPosts() {
        postsInteractor.getPosts()
            .doOnSubscribe {
                viewState.showLoading(true)
            }
            .doAfterTerminate {
                viewState.showLoading(false)
            }
            .subscribe({
                viewState.showPosts(it)
            }, { t ->
                errorHandler.proceed(t) { message, noInternet ->
                    if (noInternet) viewState.showErrorView()
                }
            })
            .connect()
    }

    fun onListStyleSelected(style: BaseItemStyle) {
        viewState.changeListStyle(style)
    }

    fun onItemClicked(item: PostResult) {
        router.navigateTo(Screens.POST_DETAILS_SCREEN(id = item.id))
    }
}