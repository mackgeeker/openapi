package com.nazariishostachuk.openapi.presentation.photos

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.nazariishostachuk.openapi.model.data.system.AddToEndSingleByTagStateStrategy
import com.nazariishostachuk.openapi.model.entity.net.photo.Photo
import com.nazariishostachuk.openapi.ui.global.BaseItemStyle

@StateStrategyType(AddToEndSingleByTagStateStrategy::class)
interface PhotosView : MvpView {
    fun showPhotos(photos: List<Photo>)
    fun showLoading(enable: Boolean)
    fun changeListStyle(style: BaseItemStyle)
}