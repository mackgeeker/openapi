package com.nazariishostachuk.openapi.presentation.app

import com.arellomobile.mvp.InjectViewState
import com.nazariishostachuk.openapi.Screens
import com.nazariishostachuk.openapi.presentation.base.BasePresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class AppPresenter(val router: Router) : BasePresenter<AppView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.setBackgroundColor()
    }

    fun onAppLoaded() {
        router.newRootScreen(Screens.POSTS_SCREEN())
    }

}