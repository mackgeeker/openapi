package com.nazariishostachuk.openapi.presentation.photos

import com.arellomobile.mvp.InjectViewState
import com.nazariishostachuk.openapi.Screens
import com.nazariishostachuk.openapi.model.entity.net.photo.Photo
import com.nazariishostachuk.openapi.model.interactor.photo.PhotoInteractor
import com.nazariishostachuk.openapi.presentation.base.BasePresenter
import com.nazariishostachuk.openapi.presentation.base.ErrorHandler
import com.nazariishostachuk.openapi.ui.global.BaseItemStyle
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class PhotosPresenter @Inject constructor(
    private val router: Router,
    private val photoInteractor: PhotoInteractor,
    private val errorHandler: ErrorHandler
) : BasePresenter<PhotosView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadPhotos()
    }

    fun loadPhotos() {
        photoInteractor.getPhotos()
            .doOnSubscribe {
                viewState.showLoading(true)
            }
            .doAfterTerminate {
                viewState.showLoading(false)
            }
            .subscribe({
                viewState.showPhotos(photos = it)
            }, { t ->
                errorHandler.proceed(t) { message, noInternet ->

                }
            })
            .connect()
    }

    fun onListStyleSelected(style: BaseItemStyle) {
        viewState.changeListStyle(style)
    }

    fun onItemClicked(photo: Photo) {
        router.navigateTo(Screens.PHOTO_DETAILS_SCREEN(photo.id))
    }
}