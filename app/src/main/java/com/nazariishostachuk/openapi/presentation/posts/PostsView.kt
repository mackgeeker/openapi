package com.nazariishostachuk.openapi.presentation.posts

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.nazariishostachuk.openapi.model.data.system.AddToEndSingleByTagStateStrategy
import com.nazariishostachuk.openapi.model.entity.net.posts.PostResult
import com.nazariishostachuk.openapi.ui.global.BaseItemStyle

@StateStrategyType(AddToEndSingleByTagStateStrategy::class)
interface PostsView : MvpView {
    fun showPosts(items: List<PostResult>)
    fun showLoading(enable: Boolean)
    fun changeListStyle(style: BaseItemStyle)
    fun showErrorView()
}