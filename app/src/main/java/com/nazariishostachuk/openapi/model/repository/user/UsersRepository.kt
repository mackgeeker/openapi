package com.nazariishostachuk.openapi.model.repository.user

import com.nazariishostachuk.openapi.model.data.system.SchedulersProvider
import com.nazariishostachuk.openapi.model.datasource.users.UsersDataSource
import com.nazariishostachuk.openapi.model.datasource.users.UsersLocalDataSource
import com.nazariishostachuk.openapi.model.repository.base.BaseRepository

class UsersRepository(
    private val api: UsersDataSource,
    private val local: UsersLocalDataSource,
    private val schedulersProvider: SchedulersProvider
) : BaseRepository(schedulersProvider) {

    fun getUserById(id: String) =
        handleSources(
            api.getUserById(id)
                .flatMap { local.saveUser(it) },
            local.getUserById(id)
        )
            .runInIoToUi()

}