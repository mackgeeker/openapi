package com.nazariishostachuk.openapi.model.data.network.api

import com.nazariishostachuk.openapi.model.entity.net.album.AlbumResponse
import com.nazariishostachuk.openapi.model.entity.net.comments.CommentsResponse
import com.nazariishostachuk.openapi.model.entity.net.photo.PhotoResponse
import com.nazariishostachuk.openapi.model.entity.net.photo.PhotosResponse
import com.nazariishostachuk.openapi.model.entity.net.posts.PostResponse
import com.nazariishostachuk.openapi.model.entity.net.posts.PostsResponse
import com.nazariishostachuk.openapi.model.entity.net.user.UserResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("public-api/photos?_format=json")
    fun getPhotos(): Single<PhotosResponse>

    @GET("public-api/photos/{id}/?_format=json")
    fun getPhotoById(@Path("id") id: String): Single<PhotoResponse>

    @GET("public-api/albums/{id}/?_format=json")
    fun getAlbumById(@Path("id") id: String): Single<AlbumResponse>

    @GET("public-api/users/{id}?_format=json")
    fun getUserById(@Path("id") id: String): Single<UserResponse>

    @GET("public-api/posts?_format=json")
    fun getPosts(): Single<PostsResponse>

    @GET("public-api/posts/{id}/?_format=json")
    fun getPostById(@Path("id") id: String): Single<PostResponse>

    @GET("public-api/comments?_format=json")
    fun getCommentsByPostId(@Query("post_id") post_id: String): Single<CommentsResponse>
}