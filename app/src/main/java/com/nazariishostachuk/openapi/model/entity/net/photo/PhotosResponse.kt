package com.nazariishostachuk.openapi.model.entity.net.photo

data class PhotosResponse(val result: List<Photo>)