package com.nazariishostachuk.openapi.model.entity.net.user

data class Links(
    val self: Link,
    val avatar: Link
)