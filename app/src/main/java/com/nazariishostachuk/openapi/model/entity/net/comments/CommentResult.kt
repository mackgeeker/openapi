package com.nazariishostachuk.openapi.model.entity.net.comments

data class CommentResult(
    val id: String,
    val name: String,
    val email: String,
    val body: String
)