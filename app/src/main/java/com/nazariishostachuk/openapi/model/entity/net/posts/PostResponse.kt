package com.nazariishostachuk.openapi.model.entity.net.posts

data class PostResponse(val result: PostResult)