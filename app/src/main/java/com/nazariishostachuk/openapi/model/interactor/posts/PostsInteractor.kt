package com.nazariishostachuk.openapi.model.interactor.posts

import com.nazariishostachuk.openapi.model.entity.net.posts.PostResult
import com.nazariishostachuk.openapi.model.repository.posts.PostsRepository
import com.nazariishostachuk.openapi.model.repository.user.UsersRepository
import io.reactivex.Single

class PostsInteractor(
    private val postsRepository: PostsRepository,
    private val usersRepository: UsersRepository
) {

    fun getPosts() = postsRepository.getPosts()
        .flatMap {
            val singles = it.map { post -> fillPostData(post) }
            Single.zip(singles) { photoArray ->
                photoArray.map { it as PostResult }
            }
        }

    fun getPostById(post_id: String) =
        postsRepository.getPostById(post_id)
            .flatMap { t -> fillPostData(t) }

    private fun fillPostData(post: PostResult): Single<PostResult> =
        usersRepository.getUserById(post.user_id)
            .map { it }
            .doOnSuccess { post.user = it }
            .map { post }

}