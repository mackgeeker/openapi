package com.nazariishostachuk.openapi.model.datasource.posts

import com.nazariishostachuk.openapi.model.entity.net.posts.PostResult
import io.reactivex.Single

interface PostsDataSource {

    fun getPosts(): Single<List<PostResult>>

    fun getPostById(id: String): Single<PostResult>

}

interface PostsLocalDataSource : PostsDataSource {
    fun savePosts(items: List<PostResult>): Single<List<PostResult>>
}