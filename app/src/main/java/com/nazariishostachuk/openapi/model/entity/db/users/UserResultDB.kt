package com.nazariishostachuk.openapi.model.entity.db.users

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class UserResultDB(
    @field:PrimaryKey
    var id: String? = null,
    var name: String? = null,
    var gender: String? = null,
    var dob: String? = null,
    var email: String? = null,
    var phone: String? = null,
    var website: String? = null,
    var address: String? = null,
    var status: String? = null,
    var _links: LinksDB? = null
) : RealmObject()