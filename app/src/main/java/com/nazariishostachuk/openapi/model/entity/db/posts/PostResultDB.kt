package com.nazariishostachuk.openapi.model.entity.db.posts

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class PostResultDB(
    @field:PrimaryKey
    var id: String? = null,
    var user_id: String? = null,
    var title: String? = null,
    var body: String? = null
) : RealmObject()