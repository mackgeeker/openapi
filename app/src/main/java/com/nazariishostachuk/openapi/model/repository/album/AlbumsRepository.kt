package com.nazariishostachuk.openapi.model.repository.album

import com.nazariishostachuk.openapi.model.data.network.api.ApiService
import com.nazariishostachuk.openapi.model.data.system.SchedulersProvider
import com.nazariishostachuk.openapi.model.repository.base.BaseRepository

class AlbumsRepository(
    private val api: ApiService,
    private val schedulersProvider: SchedulersProvider
) : BaseRepository(schedulersProvider) {

    fun getAlbumById(id: String) =
        api.getAlbumById(id)
            .runInIoToUi()
}