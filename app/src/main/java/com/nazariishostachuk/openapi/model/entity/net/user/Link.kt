package com.nazariishostachuk.openapi.model.entity.net.user

data class Link(val href : String)