package com.nazariishostachuk.openapi.model.datasource.posts

import com.nazariishostachuk.openapi.model.data.system.DataNotExistException
import com.nazariishostachuk.openapi.model.entity.db.posts.PostResultDB
import com.nazariishostachuk.openapi.model.entity.net.posts.PostResult
import com.vicpin.krealmextensions.queryAllAsSingle
import com.vicpin.krealmextensions.queryFirst
import com.vicpin.krealmextensions.saveAll
import io.reactivex.Single

class PostsLocalDS : PostsLocalDataSource {

    override fun getPosts(): Single<List<PostResult>> =
        PostResultDB().queryAllAsSingle().map {
            it.map { postResultDB -> postResultDB.mapFromDb() }
        }.map { t ->
            if (t.isEmpty()) throw DataNotExistException()
            return@map t
        }

    override fun getPostById(id: String): Single<PostResult> =
        Single.fromCallable {
            val d = PostResultDB().queryFirst { equalTo("id", id) }?.mapFromDb()

            return@fromCallable d ?: throw DataNotExistException()
        }

    override fun savePosts(items: List<PostResult>): Single<List<PostResult>> =
        Single.fromCallable {
            items.map { postResult -> postResult.mapFromNet() }.saveAll()
            return@fromCallable items
        }

    private fun PostResult.mapFromNet(): PostResultDB =
        PostResultDB(
            id = this.id,
            title = this.title,
            body = this.body,
            user_id = this.user_id
        )

    private fun PostResultDB.mapFromDb(): PostResult =
        PostResult(
            id = this.id!!,
            title = this.title!!,
            body = this.body!!,
            user_id = this.user_id!!
        )
}
