package com.nazariishostachuk.openapi.model.entity.net.posts

data class PostsResponse(val result: List<PostResult>)