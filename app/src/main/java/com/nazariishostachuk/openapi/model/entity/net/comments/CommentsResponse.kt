package com.nazariishostachuk.openapi.model.entity.net.comments

data class CommentsResponse(val result: List<CommentResult>)