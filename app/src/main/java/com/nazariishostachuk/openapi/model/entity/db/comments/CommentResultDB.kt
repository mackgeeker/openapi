package com.nazariishostachuk.openapi.model.entity.db.comments

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class CommentResultDB(
    @field:PrimaryKey
    var id: String? = null,
    var name: String? = null,
    var email: String? = null,
    var body: String? = null,
    var post_id: String? = null
) : RealmObject()