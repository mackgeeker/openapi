package com.nazariishostachuk.openapi.model.interactor.photo

import com.nazariishostachuk.openapi.model.entity.net.photo.Photo
import com.nazariishostachuk.openapi.model.repository.album.AlbumsRepository
import com.nazariishostachuk.openapi.model.repository.photo.PhotoRepository
import com.nazariishostachuk.openapi.model.repository.user.UsersRepository
import io.reactivex.Single

class PhotoInteractor(
    private val photoRepository: PhotoRepository,
    private val albumsRepository: AlbumsRepository,
    private val usersRepository: UsersRepository
) {

    fun getPhotos(): Single<List<Photo>> =
        photoRepository.getPhotos()
            .flatMap {
                val singles = it.map { photo -> fillPhotoData(photo) }
                Single.zip(singles) { photoArray ->
                    photoArray.map { it as Photo }
                }
            }

    fun getPhotoById(photo_id: String) =
        photoRepository.getPhotoById(photo_id)
            .flatMap { fillPhotoData(it.result) }

    private fun fillPhotoData(photo: Photo): Single<Photo> =
        albumsRepository.getAlbumById(photo.album_id)
            .map { it.result }
            .doOnSuccess { photo.album = it }
            .flatMap { usersRepository.getUserById(it.user_id) }
            .map { it }
            .doOnSuccess { photo.album?.user = it }
            .map { photo }

}