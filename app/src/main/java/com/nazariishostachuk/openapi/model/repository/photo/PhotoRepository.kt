package com.nazariishostachuk.openapi.model.repository.photo

import com.nazariishostachuk.openapi.model.data.network.api.ApiService
import com.nazariishostachuk.openapi.model.data.system.SchedulersProvider
import com.nazariishostachuk.openapi.model.entity.net.photo.Photo
import com.nazariishostachuk.openapi.model.repository.base.BaseRepository
import io.reactivex.Single

class PhotoRepository(
    private val api: ApiService,
    private val schedulersProvider: SchedulersProvider
) : BaseRepository(schedulersProvider) {

    fun getPhotos(): Single<MutableList<Photo>> =
        api.getPhotos()
            .map { t ->
                t.result.toMutableList()
            }
            .runInIoToUi()

    fun getPhotoById(photo_id: String) =
        api.getPhotoById(photo_id)
            .runInIoToUi()


}