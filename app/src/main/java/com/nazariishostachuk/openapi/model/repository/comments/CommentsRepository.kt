package com.nazariishostachuk.openapi.model.repository.comments

import com.nazariishostachuk.openapi.model.data.system.SchedulersProvider
import com.nazariishostachuk.openapi.model.datasource.comments.CommentsDataSource
import com.nazariishostachuk.openapi.model.datasource.comments.CommentsLocalDataSource
import com.nazariishostachuk.openapi.model.repository.base.BaseRepository

class CommentsRepository(
    private val api: CommentsDataSource,
    private val local: CommentsLocalDataSource,
    private val schedulersProvider: SchedulersProvider
) : BaseRepository(schedulersProvider) {

    fun getCommentsByPostId(post_id: String) =
        handleSources(
            fromNet = api.getCommentsByPostId(post_id)
                .onErrorResumeNext { local.getCommentsByPostId(post_id) }
                .flatMap { local.saveComments(it, post_id) },
            fromCache = local.getCommentsByPostId(post_id)
        )
            .runInIoToUi()

}