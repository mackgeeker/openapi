package com.nazariishostachuk.openapi.model.datasource.comments

import com.nazariishostachuk.openapi.model.entity.db.comments.CommentResultDB
import com.nazariishostachuk.openapi.model.entity.net.comments.CommentResult
import com.vicpin.krealmextensions.queryAsSingle
import com.vicpin.krealmextensions.saveAll
import io.reactivex.Single

class CommentsLocalDS : CommentsLocalDataSource {

    override fun getCommentsByPostId(id: String): Single<List<CommentResult>> =
        CommentResultDB().queryAsSingle { equalTo("post_id", id) }
            .map {
                it.map { commentResultDB -> commentResultDB.mapFromDb() }
            }

    override fun saveComments(items: List<CommentResult>, post_id: String): Single<List<CommentResult>> =
        Single.fromCallable {
            items.map { commentResult -> commentResult.mapFromNet(post_id) }.saveAll()
            return@fromCallable items
        }

    private fun CommentResult.mapFromNet(post_id: String): CommentResultDB =
        CommentResultDB(
            id = this.id,
            name = this.name,
            email = this.email,
            body = this.body,
            post_id = post_id
        )

    private fun CommentResultDB.mapFromDb(): CommentResult =
        CommentResult(
            id = this.id!!,
            name = this.name!!,
            email = this.email!!,
            body = this.body!!
        )
}