package com.nazariishostachuk.openapi.model.interactor.comments

import com.nazariishostachuk.openapi.model.entity.net.comments.CommentResult
import com.nazariishostachuk.openapi.model.repository.comments.CommentsRepository
import io.reactivex.Single

class CommentsInteractor(
    private val commentsRepository: CommentsRepository
) {

    fun getCommentsByPostId(post_id: String): Single<List<CommentResult>> =
        commentsRepository.getCommentsByPostId(post_id)
            .map { it }

}