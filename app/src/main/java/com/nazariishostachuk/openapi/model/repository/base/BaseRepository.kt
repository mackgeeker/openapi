package com.nazariishostachuk.openapi.model.repository.base

import com.nazariishostachuk.openapi.App
import com.nazariishostachuk.openapi.model.data.system.ConnectionHelper
import com.nazariishostachuk.openapi.model.data.system.SchedulersProvider
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

open class BaseRepository(
    private val schedulers: SchedulersProvider,
    private val connectionHelper: ConnectionHelper = App.i.DI().connectionHelper
) {

    fun <T> Single<T>.runInIoToUi(): Single<T> = this
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())

    fun <T> Observable<T>.runInIoToUi(): Observable<T> = this
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())

    fun Completable.runInIoToUi(): Completable = this
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())

    fun <T> handleSources(fromNet: T, fromCache: T): T =
        when (connectionHelper.isOnline()) {
            true -> fromNet
            false -> fromCache
        }

}
