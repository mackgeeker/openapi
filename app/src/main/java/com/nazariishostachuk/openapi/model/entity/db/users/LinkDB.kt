package com.nazariishostachuk.openapi.model.entity.db.users

import io.realm.RealmObject

open class LinkDB(
    var href: String? = null
) : RealmObject()