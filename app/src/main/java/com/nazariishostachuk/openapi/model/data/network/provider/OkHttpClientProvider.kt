package com.nazariishostachuk.openapi.model.data.network.provider

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class OkHttpClientProvider @Inject constructor() {

    private val ACCESS_TOKEN = "Cc89o2eDdEYH6iJBA9P57zmaWUok1LJKVYGV"

    companion object {
        private var TIMEOUT = 30L
    }

    fun get(): OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(TIMEOUT, TimeUnit.SECONDS)
        .addInterceptor(getLoggingInterceptor())
        .addInterceptor(provideAuthInterceptor())
        .addInterceptor(provideCacheSettingsInterceptor())
        .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
        .build()

    private fun getLoggingInterceptor(): Interceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    private fun provideCacheSettingsInterceptor(): Interceptor =
        Interceptor { chain ->
            val request = chain.request()
            request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7)
                .build()
            chain.proceed(request)
        }

    private fun provideAuthInterceptor(): Interceptor =
        Interceptor { chain ->
            val newRequest = chain.request().newBuilder()
                .addHeader("Authorization", "Bearer $ACCESS_TOKEN")
                .build()
            chain.proceed(newRequest)
        }

}
