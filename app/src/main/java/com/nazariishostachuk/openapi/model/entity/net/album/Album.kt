package com.nazariishostachuk.openapi.model.entity.net.album

import com.nazariishostachuk.openapi.model.entity.net.user.UserResult

data class Album(
    val id: String,
    val user_id: String,
    val title: String,
    var user: UserResult? = null
)