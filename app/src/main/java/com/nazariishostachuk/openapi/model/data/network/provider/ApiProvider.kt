package com.nazariishostachuk.openapi.model.data.network.provider

import com.google.gson.Gson
import com.nazariishostachuk.openapi.model.data.network.api.ApiService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class ApiProvider @Inject constructor(
    private var okHttpClient: OkHttpClient,
    private var gson: Gson
) {

    val baseUrl = "https://gorest.co.in/"

    fun getApiService() = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpClient)
        .baseUrl(baseUrl)
        .build()
        .create(ApiService::class.java)

}