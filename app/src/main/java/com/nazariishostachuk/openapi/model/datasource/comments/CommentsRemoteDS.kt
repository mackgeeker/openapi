package com.nazariishostachuk.openapi.model.datasource.comments

import com.nazariishostachuk.openapi.model.data.network.api.ApiService
import com.nazariishostachuk.openapi.model.entity.net.comments.CommentResult
import io.reactivex.Single

class CommentsRemoteDS(
    private val apiService: ApiService
) : CommentsDataSource {

    override fun getCommentsByPostId(id: String): Single<List<CommentResult>> =
        apiService.getCommentsByPostId(id)
            .map { it.result }

}