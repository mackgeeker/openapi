package com.nazariishostachuk.openapi.model.datasource.users

import com.nazariishostachuk.openapi.model.data.network.api.ApiService
import com.nazariishostachuk.openapi.model.entity.net.user.UserResult
import io.reactivex.Single

class UsersRemoteDS(
    private val apiService: ApiService
) : UsersDataSource {

    override fun getUserById(id: String): Single<UserResult> =
        apiService.getUserById(id)
            .map { it.result }
}