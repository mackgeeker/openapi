package com.nazariishostachuk.openapi.model.entity.db.users

import io.realm.RealmObject

open class LinksDB(
    var self: LinkDB? = null,
    var avatar: LinkDB? = null
) : RealmObject()