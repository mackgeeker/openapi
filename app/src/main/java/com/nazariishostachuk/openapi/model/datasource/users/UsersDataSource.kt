package com.nazariishostachuk.openapi.model.datasource.users

import com.nazariishostachuk.openapi.model.entity.net.user.UserResult
import io.reactivex.Single

interface UsersDataSource {

    fun getUserById(id: String): Single<UserResult>

}

interface UsersLocalDataSource : UsersDataSource {

    fun saveUser(it: UserResult): Single<UserResult>

}