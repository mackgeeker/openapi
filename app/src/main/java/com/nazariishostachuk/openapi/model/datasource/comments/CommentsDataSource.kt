package com.nazariishostachuk.openapi.model.datasource.comments

import com.nazariishostachuk.openapi.model.entity.net.comments.CommentResult
import io.reactivex.Single

interface CommentsDataSource {
    fun getCommentsByPostId(id: String): Single<List<CommentResult>>
}

interface CommentsLocalDataSource : CommentsDataSource {
    fun saveComments(items: List<CommentResult>,post_id: String): Single<List<CommentResult>>
}