package com.nazariishostachuk.openapi.model.entity.net.user

data class UserResult(
    val id: String,
    val name: String,
    val gender: String,
    val dob: String,
    val email: String,
    val phone: String,
    val website: String,
    val address: String,
    val status: String,
    val _links: Links
)