package com.nazariishostachuk.openapi.model.datasource.users

import com.nazariishostachuk.openapi.model.data.system.DataNotExistException
import com.nazariishostachuk.openapi.model.entity.db.users.LinkDB
import com.nazariishostachuk.openapi.model.entity.db.users.LinksDB
import com.nazariishostachuk.openapi.model.entity.db.users.UserResultDB
import com.nazariishostachuk.openapi.model.entity.net.user.Link
import com.nazariishostachuk.openapi.model.entity.net.user.Links
import com.nazariishostachuk.openapi.model.entity.net.user.UserResult
import com.vicpin.krealmextensions.queryFirst
import com.vicpin.krealmextensions.save
import io.reactivex.Single

class UsersLocalDS : UsersLocalDataSource {

    override fun getUserById(id: String): Single<UserResult> =
        Single.fromCallable {
            UserResultDB().queryFirst { equalTo("id", id) }?.mapFromDb() ?: throw DataNotExistException()
        }

    override fun saveUser(it: UserResult): Single<UserResult> = Single.fromCallable {
        it.mapFromNet().save()
        return@fromCallable it
    }

    private fun UserResult.mapFromNet(): UserResultDB =
        UserResultDB(
            id = this.id,
            name = this.name,
            address = this.address,
            dob = this.dob,
            email = this.email,
            gender = this.gender,
            phone = this.phone,
            status = this.status,
            website = this.website,
            _links = LinksDB(
                self = LinkDB(href = this._links.self.href),
                avatar = LinkDB(href = this._links.avatar.href)
            )
        )

    private fun UserResultDB.mapFromDb(): UserResult =
        UserResult(
            id = this.id!!,
            name = this.name!!,
            address = this.address!!,
            dob = this.dob!!,
            email = this.email!!,
            gender = this.gender!!,
            phone = this.phone!!,
            status = this.status!!,
            website = this.website!!,
            _links = Links(
                self = Link(href = this._links!!.self!!.href!!),
                avatar = Link(href = this._links!!.avatar!!.href!!)
            )
        )
}
