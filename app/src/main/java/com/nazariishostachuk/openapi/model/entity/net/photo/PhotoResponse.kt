package com.nazariishostachuk.openapi.model.entity.net.photo

data class PhotoResponse(val result: Photo)