package com.nazariishostachuk.openapi.model.entity.net.photo

import com.nazariishostachuk.openapi.model.entity.net.album.Album

data class Photo(
    val id: String,
    val album_id: String,
    val title: String,
    val url: String,
    val thumbnail: String,
    var album: Album? = null
)