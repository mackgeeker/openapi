package com.nazariishostachuk.openapi.model.repository.posts

import com.nazariishostachuk.openapi.model.data.system.SchedulersProvider
import com.nazariishostachuk.openapi.model.datasource.posts.PostsDataSource
import com.nazariishostachuk.openapi.model.datasource.posts.PostsLocalDataSource
import com.nazariishostachuk.openapi.model.entity.net.posts.PostResult
import com.nazariishostachuk.openapi.model.repository.base.BaseRepository

class PostsRepository(
    private val api: PostsDataSource,
    private val local: PostsLocalDataSource,
    private val schedulersProvider: SchedulersProvider
) : BaseRepository(schedulersProvider) {

    fun getPosts() =
        handleSources(
            api.getPosts()
                .onErrorResumeNext { local.getPosts() }
                .flatMap { savePosts(it) }
            , local.getPosts()
        ).runInIoToUi()

    fun getPostById(post_id: String) =
        handleSources(
            api.getPostById(post_id)
                .onErrorResumeNext { local.getPostById(post_id) },
            local.getPostById(post_id)
        )
            .runInIoToUi()

    private fun savePosts(posts: List<PostResult>) =
        local.savePosts(posts)
}