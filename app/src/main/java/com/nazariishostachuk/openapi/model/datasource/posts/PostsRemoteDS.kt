package com.nazariishostachuk.openapi.model.datasource.posts

import com.nazariishostachuk.openapi.model.data.network.api.ApiService
import com.nazariishostachuk.openapi.model.entity.net.posts.PostResult
import io.reactivex.Single

class PostsRemoteDS(
    private val apiService: ApiService
) : PostsDataSource {

    override fun getPosts(): Single<List<PostResult>> =
        apiService.getPosts()
            .map { it.result }

    override fun getPostById(id: String): Single<PostResult> =
        apiService.getPostById(id)
            .map { it.result }
}