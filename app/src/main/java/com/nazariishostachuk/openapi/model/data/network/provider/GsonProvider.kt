package com.nazariishostachuk.openapi.model.data.network.provider

import com.google.gson.GsonBuilder
import javax.inject.Inject

class GsonProvider @Inject constructor() {
    fun get() = GsonBuilder().create()
}