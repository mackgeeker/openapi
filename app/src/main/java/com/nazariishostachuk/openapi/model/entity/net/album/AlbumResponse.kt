package com.nazariishostachuk.openapi.model.entity.net.album

data class AlbumResponse(
    val result: Album
)