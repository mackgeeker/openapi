package com.nazariishostachuk.openapi.model.entity.net.posts

import com.nazariishostachuk.openapi.model.entity.net.user.UserResult

data class PostResult(
    val id: String,
    val user_id: String,
    val title: String,
    val body: String,
    var user: UserResult? = null
)