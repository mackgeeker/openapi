package com.nazariishostachuk.openapi

import android.content.Context
import com.bumptech.glide.Glide
import com.nazariishostachuk.openapi.model.data.network.api.ApiService
import com.nazariishostachuk.openapi.model.data.network.provider.ApiProvider
import com.nazariishostachuk.openapi.model.data.network.provider.GsonProvider
import com.nazariishostachuk.openapi.model.data.network.provider.OkHttpClientProvider
import com.nazariishostachuk.openapi.model.data.system.AppSchedulers
import com.nazariishostachuk.openapi.model.data.system.ConnectionHelper
import com.nazariishostachuk.openapi.model.data.system.ResourceManager
import com.nazariishostachuk.openapi.model.data.system.SchedulersProvider
import com.nazariishostachuk.openapi.model.datasource.comments.CommentsDataSource
import com.nazariishostachuk.openapi.model.datasource.comments.CommentsLocalDS
import com.nazariishostachuk.openapi.model.datasource.comments.CommentsLocalDataSource
import com.nazariishostachuk.openapi.model.datasource.comments.CommentsRemoteDS
import com.nazariishostachuk.openapi.model.datasource.posts.PostsDataSource
import com.nazariishostachuk.openapi.model.datasource.posts.PostsLocalDS
import com.nazariishostachuk.openapi.model.datasource.posts.PostsLocalDataSource
import com.nazariishostachuk.openapi.model.datasource.posts.PostsRemoteDS
import com.nazariishostachuk.openapi.model.datasource.users.UsersDataSource
import com.nazariishostachuk.openapi.model.datasource.users.UsersLocalDS
import com.nazariishostachuk.openapi.model.datasource.users.UsersLocalDataSource
import com.nazariishostachuk.openapi.model.datasource.users.UsersRemoteDS
import com.nazariishostachuk.openapi.model.interactor.comments.CommentsInteractor
import com.nazariishostachuk.openapi.model.interactor.photo.PhotoInteractor
import com.nazariishostachuk.openapi.model.interactor.posts.PostsInteractor
import com.nazariishostachuk.openapi.model.repository.album.AlbumsRepository
import com.nazariishostachuk.openapi.model.repository.comments.CommentsRepository
import com.nazariishostachuk.openapi.model.repository.photo.PhotoRepository
import com.nazariishostachuk.openapi.model.repository.posts.PostsRepository
import com.nazariishostachuk.openapi.model.repository.user.UsersRepository
import com.nazariishostachuk.openapi.presentation.base.ErrorHandler
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

class Dependencies(context: Context) {
    val resourceManager by lazy { ResourceManager(context) }
    val connectionHelper by lazy { ConnectionHelper(context) }
    val errorHandler by lazy { ErrorHandler(resourceManager) }
    val schedulersProvider: SchedulersProvider by lazy { AppSchedulers() }

    private val cicerone: Cicerone<Router> by lazy { Cicerone.create() }
    val router: Router by lazy { cicerone.router }
    val navigatorHolder: NavigatorHolder by lazy { cicerone.navigatorHolder }

    val gson by lazy { GsonProvider().get() }
    val okHttpClientProvider by lazy { OkHttpClientProvider() }

    val glide by lazy { Glide.with(context) }

    val apiProvider: ApiProvider by lazy {
        ApiProvider(
            okHttpClient = okHttpClientProvider.get(),
            gson = gson
        )
    }

    val api: ApiService by lazy { apiProvider.getApiService() }

    val postsRemoteDataSource: PostsDataSource by lazy { PostsRemoteDS(api) }
    val postsLocalDataSource: PostsLocalDataSource by lazy { PostsLocalDS() }

    val usersRemoteDataSource: UsersDataSource by lazy { UsersRemoteDS(api) }
    val usersLocalDataSource: UsersLocalDataSource by lazy { UsersLocalDS() }

    val commentsRemoteDataSource: CommentsDataSource by lazy { CommentsRemoteDS(api) }
    val commentsLocalDataSource: CommentsLocalDataSource by lazy { CommentsLocalDS() }

    val photoRepository by lazy { PhotoRepository(api, schedulersProvider) }
    val albumsRepository by lazy { AlbumsRepository(api, schedulersProvider) }
    val usersRepository by lazy { UsersRepository(usersRemoteDataSource, usersLocalDataSource, schedulersProvider) }
    val commentsRepository by lazy {
        CommentsRepository(
            commentsRemoteDataSource,
            commentsLocalDataSource,
            schedulersProvider
        )
    }
    val postsRepository by lazy { PostsRepository(postsRemoteDataSource, postsLocalDataSource, schedulersProvider) }

    val photoInteractor by lazy { PhotoInteractor(photoRepository, albumsRepository, usersRepository) }
    val postsInteractor by lazy { PostsInteractor(postsRepository, usersRepository) }
    val commentsInteractor by lazy { CommentsInteractor(commentsRepository) }

}