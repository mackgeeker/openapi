package com.nazariishostachuk.openapi

import android.view.View

fun View.changeVisibility(visibility: Int) {
    if (this.visibility != visibility)
        this.visibility = visibility
}